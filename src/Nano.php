<?php
/**
 * This file is part of built by Layla Tichy.
 *
 * (c) Layla Tichy <contact@laylatichy.com>
 *                 <web@laylatichy.com>
 *
 * Copyright (c) 2020. All rights reserved
 *
 * @author    Layla Tichy
 * @copyright built by Layla Tichy
 *
 * @see       http://www.laylatichy.com
 * @see       /github => www.github.com/laylatichy
 * @see       /gitlab => www.gitlab.com/x.laylatichy.x
 */

namespace laylatichy;

use laylatichy\nano\core\Cache;
use laylatichy\nano\core\Headers;
use laylatichy\nano\core\HttpCode;
use laylatichy\nano\core\Request;
use laylatichy\nano\core\Router;
use laylatichy\nano\core\Config;
use laylatichy\nano\core\Response;
use Throwable;
use Workerman\Connection\TcpConnection;
use Workerman\Protocols\Http\Request as WorkermanRequest;
use Workerman\Worker;

/**
 * Nano::class.
 */
class Nano extends Worker {
    /**
     * Name of the worker processes.
     *
     * @var string
     */
    public          $name = 'nano';

    public Headers  $headers;

    public Response $response;

    public Request  $request;

    private Config  $config;

    private Router  $router;

    private Cache   $cache;

    public function __construct(
        protected string $socket = '',
        protected array $options = [],
    ) {
        $this->config = Config::getInstance();
        $this->clean();

        if ($this->socket === '') {
            $this->socket = $this->getSocket();
        }

        $this->router   = new Router();
        $this->headers  = new Headers();
        $this->response = new Response();
        $this->cache    = new Cache();

        $this->processes();

        $this->cache();

        parent::__construct($this->socket, $this->options);

        $this->onMessage = [$this, 'onMessage',];
    }

    public function onMessage(TcpConnection $connection, WorkermanRequest $request): void {
        if ($request->method() === Request::OPTIONS) {
            $connection->send(
                $this->response->send(
                    code: HttpCode::OK,
                    headers: $this->headers->getAll(),
                )
            );

            return;
        }

        static $callbacks = [];

        try {
            $callback = $callbacks[$request->path()] ?? null;
            if ($callback && $this->cache->isEnabled()) {
                if (time() < $callback['timestamp'] + $this->cache->getDuration()) {
                    $connection->send(
                        $this->response->send(
                            code: $callback['code'],
                            headers: $callback['headers'],
                            body: $callback['response'],
                        )
                    );

                    return;
                }
                unset($callbacks[$request->path()]);
            }

            $response = $this->router->dispatch(
                method: $request->method(),
                path: $request->path()
            );

            if ($response) {
                $callback = $response[1];
                if (!empty($response[2])) {
                    $args     = array_values(array: $response[2]);
                    $callback = function ($request) use ($args, $callback) {
                        return $callback($request, ...$args);
                    };
                }
                $this->request = new Request(request: $request);

                $callback($request);

                if ($this->cache->isEnabled()) {
                    $callbacks[$request->path()] = [
                        'callback'  => $callback,
                        'code'      => $this->response->getCode(),
                        'headers'   => $this->headers->getAll(),
                        'response'  => $this->response->getBody(),
                        'timestamp' => time(),
                    ];
                }

                $connection->send(
                    $this->response->send(
                        code: $this->response->getCode(),
                        headers: $this->headers->getAll(),
                        body: $this->response->getBody()
                    )
                );

                return;
            }
            $this->response->code(code: HttpCode::NOT_FOUND);
            $connection->send(
                $this->response->send(
                    code: $this->response->getCode(),
                    headers: $this->headers->getAll(),
                    body: $this->response->notFound()
                )
            );
        } catch (Throwable $e) {
            $this->response->code(code: HttpCode::INTERNAL_SERVER_ERROR);
            $connection->send(
                $this->response->send(
                    code: $this->response->getCode(),
                    headers: $this->headers->getAll(),
                    body: (string)$e
                )
            );
            echo $e;
        }
    }

    public function start(): void {
        $this->router->init();
        parent::runAll();
    }

    public function get(string $path, object $callback): void {
        $this->router->get(path: $path, callback: $callback);
    }

    public function post(string $path, object $callback): void {
        $this->router->post(path: $path, callback: $callback);
    }

    public function response(int $code, array $data): void {
        $this->response->code(code: $code);
        $this->response->json(body: $data);
    }

    public function status(): void {
        $this->response->code(code: HttpCode::OK);
        $this->response->json(
            body: [
                'code'         => HttpCode::OK,
                'hostname'     => $_SERVER['HOSTNAME'] ?? $this->getSocket(),
                'requestTime'  => $_SERVER['REQUEST_TIME'],
                'workersCount' => count(
                    value: static::getAllWorkers()
                ),
            ]
        );
    }

    public function processes(int $count = null): void {
        $nproc       = (int)shell_exec(command: 'nproc') * 2;
        $this->count = $count ?? ($nproc ?? 8);
    }

    public function cache(bool $option = Cache::ENABLE, int $mins = 60): void {
        $this->cache->cache(option: $option, mins: $mins);
    }

    private function clean(): void {
        $files = glob(pattern: $this->config::getOption(option: 'root') . '/vendor/workerman/*.pid');

        is_array(value: $files) && array_map(callback: 'unlink', array: $files);
    }

    private function getSocket(): string {
        return "{$this->config::getOption(option: 'host')}:{$this->config::getOption(option: 'port')}";
    }
}
