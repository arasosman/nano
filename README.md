### laylatichy/nano

[![release](https://img.shields.io/packagist/v/laylatichy/nano?style=flat-square)](https://packagist.org/packages/laylatichy/nano)
[![SonarQube Quality](http://sonarqube.laylatichy.com/api/project_badges/measure?project=laylatichy-nano&metric=alert_status)](http://sonarqube.laylatichy.com/dashboard?id=laylatichy-nano)
[![PHPStan](https://img.shields.io/badge/PHPStan-Level%20max-brightgreen.svg?style=flat&logo=php)](https://shields.io/#/)
[![Total Downloads](https://img.shields.io/packagist/dt/laylatichy/nano?style=flat-square)](https://packagist.org/packages/laylatichy/nano)

##### CI STATUS

- dev [![pipeline status](https://gitlab.com/x.laylatichy.x/nano/badges/dev/pipeline.svg)](https://gitlab.com/x.laylatichy.x/nano/-/commits/dev)
- master [![pipeline status](https://gitlab.com/x.laylatichy.x/nano/badges/master/pipeline.svg)](https://gitlab.com/x.laylatichy.x/nano/-/commits/master)
- release [![pipeline status](https://gitlab.com/x.laylatichy.x/nano/badges/release/pipeline.svg)](https://gitlab.com/x.laylatichy.x/nano/-/commits/release)

##### Coverage

- [php coverage](http://nano.laylatichy.com/nano/php/) 
- [js coverage](http://nano.laylatichy.com/nano/js/)

##### Code Analysis

- [sonarqube](http://sonarqube.laylatichy.com/dashboard?id=laylatichy-nano)

#### What is nano?

High performance micro framework based on [workerman](https://github.com/walkor/workerman) for writing APIs with php

#### Install

- `composer require laylatichy/nano`

#### Requires

PHP 8.0 or Higher

A POSIX compatible operating system

POSIX and PCNTL extensions required

Event extension recommended for better performance

#### Run

```php
php index.php start
```

#### Commands

```php
php index.php restart -d
php index.php stop
php index.php status
php index.php connections
```

#### Basic Usage

##### [Example #01](https://gitlab.com/x.laylatichy.x/nano/-/blob/master/examples/01/index.php) - basic

```php
<?php

require_once 'vendor/autoload.php';

use laylatichy\Nano;

//default settings for nano app - http://0.0.0.0:3000
$nano = new Nano();

// GET route
$nano->get(
    '/',
    function ($request) use ($nano) {
        $nano->response->code(200);
        $nano->response->plain('hello plain');
    }
);

// POST route
$nano->post(
    '/hello/{name}',
    function ($request, $name) use ($nano) {
        $nano->response->code(200);
        $nano->response->json(
            [
                'hello' => $name,
            ]
        );
    }
);

// Start nano
$nano->start();
```

#####  [Example #02](https://gitlab.com/x.laylatichy.x/nano/-/blob/master/examples/02/index.php) - custom port
```php
<?php

require_once 'vendor/autoload.php';

use laylatichy\Nano;

//lets start on a different port
$nano = new Nano('http://0.0.0.0:5000');

// GET route - http://localhost:5000/
$nano->get(
    '/',
    function ($request) use ($nano) {
        $nano->response->code(200);
        $nano->response->plain('hello from port 5000');
    }
);

// Start nano
$nano->start();
```

#####  [Example #03](https://gitlab.com/x.laylatichy.x/nano/-/blob/master/examples/03/index.php) - custom port set by a config file
```php
<?php

require_once 'vendor/autoload.php';

use laylatichy\Nano;

//lets start on a different port using .config file
$nano = new Nano();

// GET route - http://localhost:4000/
$nano->get(
    '/',
    function ($request) use ($nano) {
        $nano->response->code(200);
        $nano->response->plain('hello from port 4000 set by a config file');
    }
);

// Start nano
$nano->start();
```

#####  [Example #04](https://gitlab.com/x.laylatichy.x/nano/-/blob/master/examples/04/index.php) - use function to return a value
```php
<?php

require_once 'vendor/autoload.php';

use laylatichy\Nano;

$nano = new Nano();

// simple function using $nano
function helloNano(Nano $nano, string $name): void {
    $nano->response->code(200);
    $nano->response->plain("hello {$name}");
}

// simple function just returning a value
function hello(string $name): string {
    return "hello {$name}";
}

// GET routes - http://localhost:3000/
$nano->get(
    '/',
    function ($request) use ($nano) {
        helloNano($nano, 'test');
    }
);

$nano->get(
    '/hello',
    function ($request) use ($nano) {
        $nano->response->code(200);
        $nano->response->plain(hello("stranger!, it's me again"));
    }
);

// Start nano
$nano->start();
```
