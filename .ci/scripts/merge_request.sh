#!/bin/sh

#
# This file is part of built by Layla Tichy.
#
# (c) Layla Tichy <contact@laylatichy.com>
#                 <web@laylatichy.com>
#
# Copyright (c) 2020. All rights reserved
#
# @author    Layla Tichy
# @copyright built by Layla Tichy
#
# @see       http://www.laylatichy.com
# @see       /github => www.github.com/laylatichy
# @see       /gitlab => www.gitlab.com/x.laylatichy.x
#

MODULE="nano"
git checkout release
git reset --hard origin/release

#Get the highest tag number
VERSION=$(git describe --abbrev=0 --tags)
VERSION=${VERSION:-'0.0.0'}

#Get number parts
MAJOR="${VERSION%%.*}"
VERSION="${VERSION#*.}"
MINOR="${VERSION%%.*}"
VERSION="${VERSION#*.}"
PATCH="${VERSION%%.*}"
VERSION="${VERSION#*.}"

#Increase version
PATCH=$((PATCH + 1))

RELEASE="$MAJOR.$MINOR.$PATCH"
echo "new version $RELEASE"

BRANCH="version/""$RELEASE"

git checkout master
#git reset --hard origin/master

cd ..
mv $MODULE /tmp/
mkdir $MODULE
mv "/tmp/$MODULE/.git" "$MODULE/"
cd $MODULE || exit

echo "$BRANCH"

git checkout release
git fetch

# shellcheck disable=SC2046
if [ $(git rev-parse --quiet --verify "$BRANCH") ] >/dev/null; then
    echo "Branch $BRANCH already exists."
    git checkout "$BRANCH"
    git rm -rf .
else
    echo "Branch $BRANCH does not exists."
    git checkout -b "$BRANCH"
    git rm -rf .
fi

git branch

cd ..
mv "/tmp/$MODULE/.ci" "$MODULE/"
mv "/tmp/$MODULE/src" "$MODULE/"
mv "/tmp/$MODULE/.gitlab-ci.yml" "$MODULE/"
mv "/tmp/$MODULE/composer.json" "$MODULE/"
mv "/tmp/$MODULE/renovate.json" "$MODULE/"
mv "/tmp/$MODULE/README.md" "$MODULE/"

cd $MODULE || exit

echo "add changes and commit"
git add -A
git commit -m "$CI_COMMIT_MESSAGE"

git push -f --set-upstream origin "$BRANCH" -o merge_request.create -o merge_request.target=release -o merge_request.title="version/""$RELEASE" -o merge_request.label="release ""$RELEASE" -o merge_request.remove_source_branch

