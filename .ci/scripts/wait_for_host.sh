#!/bin/bash
#
# This file is part of built by Layla Tichy.
#
# (c) Layla Tichy <contact@laylatichy.com>
#                 <web@laylatichy.com>
#
# Copyright (c) 2020. All rights reserved
#
# @author    Layla Tichy
# @copyright built by Layla Tichy
#
# @see       http://www.laylatichy.com
# @see       /github => www.github.com/laylatichy
# @see       /gitlab => www.gitlab.com/x.laylatichy.x
#

sleep 10

declare -r HOST="http://docker:10000"

wait-for-url() {
    echo "Testing $1"
    # shellcheck disable=SC2016
    # shellcheck disable=SC1004
    timeout -s TERM 45 bash -c \
    'while [[ "$(curl -s -o /dev/null -L -w ''%{http_code}'' ${0})" != "200" ]];\
    do echo "Waiting for ${0}" && sleep 2;\
    done' "${1}"
    echo "OK!"
    curl -I "$1"
}

# wait for index
wait-for-url ${HOST}
