#!/bin/sh

#
# This file is part of built by Layla Tichy.
#
# (c) Layla Tichy <contact@laylatichy.com>
#                 <web@laylatichy.com>
#
# Copyright (c) 2020. All rights reserved
#
# @author    Layla Tichy
# @copyright built by Layla Tichy
#
# @see       http://www.laylatichy.com
# @see       /github => www.github.com/laylatichy
# @see       /gitlab => www.gitlab.com/x.laylatichy.x
#

#Get the highest tag number
VERSION=$(git describe --abbrev=0 --tags)
VERSION=${VERSION:-'0.0.0'}

#Get number parts
MAJOR="${VERSION%%.*}"
VERSION="${VERSION#*.}"
MINOR="${VERSION%%.*}"
VERSION="${VERSION#*.}"
PATCH="${VERSION%%.*}"
VERSION="${VERSION#*.}"

#Increase version
PATCH=$((PATCH + 1))

#Get current hash and see if it already has a tag
GIT_COMMIT=$(git rev-parse HEAD)
NEEDS_TAG=$(git describe --contains "$GIT_COMMIT")

#Create new tag
NEW_TAG="$MAJOR.$MINOR.$PATCH"
echo "Updating to $NEW_TAG"

#Only tag if no tag already (would be better if the git describe command above could have a silent option)
if [ -z "$NEEDS_TAG" ]; then
    echo "Tagged with $NEW_TAG (Ignoring fatal:cannot describe - this means commit is untagged) "
    git tag -a "$NEW_TAG" -m "release - $NEW_TAG"
    git push --tags
else
    echo "Already a tag on this commit"
fi

echo "tagging done | ""$NEW_TAG"
